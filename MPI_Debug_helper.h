#ifndef MY_MPI_DEBUG_H
#define MY_MPI_DEBUG_H
#include <mpi.h>
#include <stdio.h>

int mpi_debug_helper_lock___=0;
MPI_Win  mpi_debug_helper_win;
extern "C"
{
    void mpi_debug_helper_release(void);
}
void mpi_debug_helper_release(void)
{
    int j=1;
    MPI_Put(&j, 1, MPI_INT, 0, 0, 1, MPI_INT, mpi_debug_helper_win);
}


namespace
{
    void print_rank_and_pid(void)
    {
        int myrank;
        int nproc;
        MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
        MPI_Comm_size(MPI_COMM_WORLD, &nproc);

        int pid=getpid();
        int* pids=new int [nproc];

        MPI_Gather(&pid, 1, MPI_INT, pids, 1, MPI_INT, 0, MPI_COMM_WORLD);

        if(myrank ==0)
        {
            for(int i=0; i< nproc;++i)
            {
                printf("Rank %d, PID %d ready for attach\n", i, pids[i]);
            }
        fflush(stdout);
        }
        delete [] pids;
    }
    void trap_and_wait(void)
    {
        MPI_Win_create(&mpi_debug_helper_lock___, 1, sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &mpi_debug_helper_win);
        MPI_Win_fence(0, mpi_debug_helper_win);
        int myrank;
        MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
        if(myrank == 0)
        {
            printf("**** instruction ****\n");
            printf("(1) attach desired rank with debugger");
            printf("  > gdb a.out {PID}\n");
            printf("(2) attach rank 0 with debugger\n");
            printf("  > gdb a.out {PID_of_Rank0}\n");
            printf("(3) please type following command in Rank 0's gdb window\n");
            printf("  (gdb) set var i = 0\n");
            printf("(4) Happy debugging!!\n");
            printf("\n");

            // wait for continue trigger
            while (mpi_debug_helper_lock___==0) {}

            MPI_Barrier(MPI_COMM_WORLD);
        }else{
            MPI_Barrier(MPI_COMM_WORLD);
        }
        MPI_Win_free(&mpi_debug_helper_win);
    }

    void my_mpi_debug(void)
    {
        //MPI_Initされてなかったらメッセージを出力して終了
        int initialized;
        MPI_Initialized(&initialized);
        if(!initialized)
        {
            fprintf(stderr, "MPI_Init is not called. please check if my_mpi_debug() is called after MPI_Init or MPI_Initthread()\n");
            return;
        }

        static bool first_call=1;

        if(first_call)
        {
            first_call=0;
            print_rank_and_pid();
            trap_and_wait();
        }
    }
}//end of namespace
#endif
