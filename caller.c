#include <mpi.h>
#include <stdio.h>

int main(int argc,char *argv[]){
  int nproc, myrank;
  MPI_Comm comm=MPI_COMM_WORLD;

  MPI_Init(&argc, &argv);
  my_mpi_debug(MPI_COMM_WORLD);

  MPI_Comm_size(comm, &nproc);
  MPI_Comm_rank(comm, &myrank);

  printf("this is Rank %d\n",myrank);

  MPI_Finalize();

  return 0;
}

